package com.onepiece.requestproxy.entity.params;

public class ParamsEntity {
	
	private String name;
	
	private String value;
	
	private int order = 0;
	
	public ParamsEntity(){
		
	}
	
	public ParamsEntity(String name,String value){
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}


}
