package com.onepiece.requestproxy.util;

import java.util.Comparator;

import com.onepiece.requestproxy.entity.params.ParamsEntity;

public class ParamsComparator implements Comparator<ParamsEntity> {

	@Override
	public int compare(ParamsEntity prve, ParamsEntity next) {
		return prve.getOrder() - next.getOrder();
	}

}
