package com.onepiece.requestproxy.util;

import com.onepiece.requestproxy.annotation.IRequestMethod;

/**
 * 获取默认注解,减少注解的写入
 * 
 * @author JueYue
 * @date 2014年8月3日 下午1:30:50
 */
public class BaseAnnotation {

	@IRequestMethod(connectTimeout = 60)
	public void requestMethod() {

	}

}
