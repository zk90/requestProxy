package com.onepiece.requestproxy.annotation;

import java.lang.annotation.*;

/**
 * Request 请求参数注解
 * @author JueYue
 * 2014年2月20日--上午10:37:02
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestParams {
	/**
	 * 参数名称
	 * 2014年5月15日
	 * @return
	 */
	 String value();
	 /**
	  * 顺序 for rest
	  * 2014年5月15日
	  * @return
	  */
	 int order() default 0;
}
