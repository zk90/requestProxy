package com.onepiece.requestproxy.annotation;

import com.onepiece.requestproxy.entity.enums.RequestEncodeEnum;
import com.onepiece.requestproxy.entity.enums.RequestResultEnum;
import com.onepiece.requestproxy.entity.enums.RequestTypeEnum;

import java.lang.annotation.*;

/**
 * 求情代理类型
 * Created by jue on 14-4-21.
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface IRequestMethod {
    /**
     * 目标URL
     * @return
     */
    String url() default "";

    /**
     * 请求类型
     * @return
     */
    RequestTypeEnum type() default RequestTypeEnum.POST;

    /**
     * 字符编码
     * @return
     */
    RequestEncodeEnum encode() default RequestEncodeEnum.UTF8;
    /**
     * 返回类型,目前XML 只支持对象类型
     * @return
     */
    RequestResultEnum result() default RequestResultEnum.JSON;
    /**
     * 是不是默认转码字符编码
     * @return
     */
    boolean isTranscoding() default true;

    /**
     * 连接超时
     * @return
     */
    int connectTimeout() default 13;

}
