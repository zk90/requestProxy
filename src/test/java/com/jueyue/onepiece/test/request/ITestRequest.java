package com.jueyue.onepiece.test.request;

import com.jueyue.onepiece.test.entity.BaiduWeatherEntity;
import com.onepiece.requestproxy.annotation.IRequest;
import com.onepiece.requestproxy.annotation.IRequestMethod;
import com.onepiece.requestproxy.annotation.RequestParams;
import com.onepiece.requestproxy.entity.enums.RequestTypeEnum;

/**
 * 测试接口
 * 
 * @author JueYue 2014年5月17日 - 上午10:23:32
 */
@IRequest("testRequest")
public interface ITestRequest {

	@IRequestMethod(type = RequestTypeEnum.GET, url = "http://api.map.baidu.com/telematics/v3/weather")
	String testGet(@RequestParams("location") String location,
			@RequestParams("output") String output,
			@RequestParams("ak") String ak);
	
	@IRequestMethod(type = RequestTypeEnum.GET, url = "http://api.map.baidu.com/telematics/v3/weather")
	BaiduWeatherEntity testGetEntity(@RequestParams("location") String location,
			@RequestParams("output") String output,
			@RequestParams("ak") String ak);

}
