#requestProxy
    requestProxy 是为了把 http,以及webserver 的简单请求进行封装

作用:

	1.可以使http请求进行接口化,使得http接口更好的和java做融合
	2.融合json可以自动转为bean
	3.也可以解析符合要求的xml
	4.负责的返回string,大家可以自己解析

使用方法:

	1.配置httpclient和spring 融合,这个网上比较多
	2.写接口与参数
	3.注入调用接口

适用项目:

	与其他项目进行交互,调用地方接口较多的项目
	
支持返回类型:

	String,int,long,double
	bean
	list<bean>
	
	

